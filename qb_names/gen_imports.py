import csv

cout = open('customers_import.csv', 'w')
cout.write('Company, ProperName,\n')
vout = open('vendors_import.csv', 'w')
           
with open('customers.csv') as customers_file:
    for line in customers_file:
        data = line.split(',')
        cout.write(data[2].replace('*', '') + ',' + data[1] + ',\r\n')
    customers_file.close()
    cout.close()
           
with open('vendors.csv') as file:
    reader = csv.DictReader(file)
    writer = csv.writer(vout, quoting=csv.QUOTE_ALL, lineterminator='\n')
    writer.writerow(['Company', 'ProperName'])
    for row in reader:
        if row['CompanyName'] == '':
            writer.writerow([row['Name'].replace('*', '').replace('ach ', ''), row['Name']])
        else:
            writer.writerow([row['CompanyName'].replace('*', '').replace('ach ', ''), row['Name']])
    vout.close()
           
