import csv
with open('VendorFees.csv', 'r') as file, open('OutFile.csv', 'w', newline='') as out_file:
        reader = csv.DictReader(file)
        writer = csv.writer(out_file, quoting=csv.QUOTE_ALL)
        fee, weight = "", 100
        for row in reader:
            if row['County'] == county and row['ProductType'] == product_type and row['Grade'] == grade:
                if row['Fee'] != fee:
                    fee = row['fee']
                    weight -= 10
            else:
                weight = 100
            company = row['Company']
            writer.writerow([row['Company'], row['County'], row['State'], row['ProductName'], row['ProductType'], row['Fee'], row['Grade'], weight])
