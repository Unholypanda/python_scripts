import json

data = json.load(open('Invoices and Bills 2018-03-14.json', 'r'))

accounts = {
    'DR_INVOICE_ITEM_MAP': {
        'Assignment Verification Report (AVR)': '8000003D-1520263806',
        'Certified Document Retrieval': '80000039-1520263640',
        'Civil Copy Retrieval w/case #': '8000003C-1520263765',
        'Civil Copy Retrieval w/o Case #': '8000003B-1520263742',
        'Deed Search Report': '80000038-1520263617',
        'Document Retrieval': '80000037-1520263600',
        'HOA Directory Search Report': '80000036-1520263579',
        'Lien Position Report': '80000035-1520263557',
        'Lien Release Search Report': '80000034-1520263538',
        'Mini Search': '80000033-1520263516',
        'Mini Search (No Copies)': '8000003A-1520263712',
        'Research Report': '80000032-1520263489',
        'UCC Search & Retrieval': '80000031-1520263465',
        'Vesting & Legal Search Report': '8000002F-1520263419',
        'Voluntary Lien Search Report': '80000030-1520263442',
        '2nd Trip Fee': '80000052-1520271783',
        'Certification Fee': '80000053-1520271811',
        'Courthouse Fee': '80000054-1520271836',
        'Document Copy Cost': '80000055-1520271860',
        'Plat Map Fee': '80000056-1520271883',
        'Wrong County Fee': '80000057-1520271904'
        },
    'REC_INVOICE_ITEM_MAP': {
        'E-Recording': '80000051-1520264848',
        'Mail In Document Recording': '80000050-1520264828',
        'UCC Lien Filing': '8000004F-1520264793',
        'Walk-In Document Recording': '8000004E-1520264770',
        '2nd Trip Fee': '8000005E-1520272161',
        'Certification Fee': '8000005F-1520272189',
        'Courthouse Fee': '80000061-1520272242',
        'Document Copy Cost': '80000062-1520272273',
        'Plat Map Fee': '80000063-1520272314',
        'Wrong County Fee': '80000064-1520272337',
        'County Recording Fee': '80000060-1520272218'
},

'TS_INVOICE_ITEM_MAP': {
    '10 Year Search': '80000048-1520264317',
    '20 Year Search': '80000047-1520264298',
    '30 Year Search': '80000046-1520264277',
    'CC&R Search Report': '80000045-1520264257',
    'Chain of Title Report': '80000044-1520264235',
    'Current Owner Ownership & Encumbrance Report': '8000004D-1520264675',
    'Easement & Restrictions Search': '8000004C-1520264642',
    'Full Search': '80000043-1520264212',
    'Historical Title Search Report': '8000004B-1520264617',
    'Judgment & Lien Search Report': '8000004A-1520264591',
    'MMR': '80000042-1520264113',
    'Property Tax Status Search Report': '80000049-1520264352',
    'SMR': '80000041-1520264016',
    'Specified Document Date Search': '8000003E-1520263934',
    'Two Owner Search Report': '80000040-1520263988',
    'Update Search Report': '8000003F-1520263962',

    '2nd Trip Fee': '80000058-1520271999',
    'Certification Fee': '80000059-1520272024',
    'Courthouse Fee': '8000005A-1520272051',
    'Document Copy Cost': '8000005B-1520272077',
    'Plat Map Fee': '8000005C-1520272101',
    'Wrong County Fee': '8000005D-1520272130'
},

'DR_BILL_ACCOUNT_MAP': {
    'Assignment Verification (AVR)': '80000104-1520259402',
    'Certified Document Retrieval': '80000103-1520259351',
    'Civil Copy Retrieval w/case #': '80000102-1520259330',
    'Civil Copy Retrieval w/o Case #': '80000101-1520259308',
    'Deed Search Report': '80000100-1520259286',
    'Document Retrieval': '800000FF-1520259264',
    'HOA Directory Search Report': '800000FE-1520259239',
    'Lien Position Report': '800000FD-1520259215',
    'Lien Release Search Report': '800000FC-1520259193',
    'Mini Search': '800000FB-1520259171',
    'Mini Search (No Copies)': '800000FA-1520259146',
    'Research Report': '800000F9-1520259121',
    'UCC Search & Retrieval': '800000F8-1520259097',
    'Vesting & Legal Search Report': '800000F7-1520259071',
    'Voluntary Lien Search Report': '800000F5-1520258779',

    'Document Copy Cost': '8000011F-1520261728',
    'Certification Fee': '80000122-1520261928',
    'Courthouse Fee': '80000125-1520262043',
    'Plat Map Fees': '8000012F-1520262973',
    '2nd Trip Fee': '80000129-1520262419',
    'Wrong County Fee': '8000012C-1520262517'
},

'TS_BILL_ACCOUNT_MAP': {
    '10 Year Search': '80000115-1520259906',
    '20 Year Search': '80000114-1520259885',
    '30 Year Search': '80000113-1520259862',
    'CC&R Search Report': '80000112-1520259840',
    'Chain of Title Report': '80000111-1520259815',
    'Current Owner Ownership & Encumbrance Report': '80000110-1520259793',
    'Easement & Restrictions Search': '8000010F-1520259770',
    'Full Search': '8000010E-1520259747',
    'Historical Title Search Report': '8000010D-1520259723',
    'Judgment & Lien Search Report': '8000010C-1520259700',
    'MMR': '8000010B-1520259677',
    'Property Tax Status Search Rpt': '8000010A-1520259653',
    'SMR': '80000109-1520259626',
    'Specified Document Date Search': '80000108-1520259603',
    'Two Owner Search Report': '80000107-1520259579',
    'Update Search Report': '80000106-1520259545',

    'Document Copy Cost': '80000120-1520261843',
    'Certification Fee': '80000123-1520261965',
    'Courthouse Fee': '80000126-1520262075',
    'Plat Map Fees': '80000130-1520263003',
    '2nd Trip Fee': '8000012A-1520262452',
    'Wrong County Fee': '8000012D-1520262542'
},

'REC_BILL_ACCOUNT_MAP': {
    'UCC Lien Filing': '80000118-1520260046',
    'Walk-In Document Recording': '80000117-1520260023',

    'Document Copy Cost': '80000121-1520261881',
    'Wrong County Fee': '8000012E-1520262573',
    'County Recording Fee': '80000128-1520262201',
    '2nd Trip Fee': '8000012B-1520262482',
    'Plat Map Fees': '80000131-1520263092',
    'Courthouse Fee': '80000127-1520262110',
    'Certification Fee': '80000124-1520262005'
}}

for key in accounts:
    for account in accounts[key]:
        print(len(accounts[key][account]))

for item in data['bills']:
    if len(item['line_items']) == 1:
            if item['line_items'][0]['type'] == 'Document Copy Cost':
                print(item['number'] + ' - Found Missing Service Fee!')
            else:
                print(item['number'] + ' - Possible missing copy fee!')
    if len(item['line_items']) == 0:
        print(item['number'] + ' - No fees found!')
                       
