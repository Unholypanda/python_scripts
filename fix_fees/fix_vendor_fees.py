with open('RWVendorFees.csv', 'r') as data:
    file = 1
    i = 1
    output = open('FixedRWVendorFees.csv', 'w')
    for line in data:
        if i >= 700000:
            output.close()
            file +=1
            i = 1
            output = open('FixedRWVendorFees' + str(file) + '.csv', 'w')
            output.write('"PartnerType","Company","County","State","ProductType","Amount","FeeName","TransactionTypeID"')
        if 'Bringdown / Update Recording' in line or 'Document Research / Recording' in line or 'Specified Document / Date' in line or 'Test' in line:
            pass
        elif 'Retrieval' in line:
            new = line.replace(',Retrieval,', ',Document Retrieval,')
            output.write(new)
            new = line.replace(',Retrieval,', ',Assignment Verification Report (AVR),')
            output.write(new)
            new = line.replace(',Retrieval,', ',SMR,')
            output.write(new)
        elif "Mini Search" in line:
            new = line.replace(',Mini Search,', ',Mini Search,')
            output.write(new)
            new = line.replace(',Mini Search,', ',MMR,')
            output.write(new)
            new = line.replace(',Mini Research,', ',Mini Search No Copies,')
            output.write(new)
        else:
            line = line.replace(',10 Yr Title Search,', ',10 Year Search,')
            line = line.replace(',20 Yr Title Search,', ',20 Year Search,')
            line = line.replace(',30 Yr Title Search,', ',30 Year Search,')
            line = line.replace(',Current Owner,', ',"Current Owner Ownership & Encumbrance Report",')
            line = line.replace(',Certified Copy,', ',Certified Document Retrieval,')
            line = line.replace(',Chain of Title Search,', ',Chain of Title Report,')
            line = line.replace(',Doc Ret Civil CN NOT Provided,', ',Civil Copy Retrieval w/o Case #,')
            line = line.replace(',Doc Ret Civil CN Provided,', ',Civil Copy Retrieval w/case #,')
            line = line.replace(',Document Recording,', ',Walk-In Document Recording,')
            line = line.replace(',Full Title Search,', ',Full Search,')
            line = line.replace(',HOA Identification,', ',HOA Directory Search Report,')
            line = line.replace(',Judgment Search,', ',Judgment & Lien Search Report,')
            line = line.replace(',Lien Release,', ',Lien Release Search Report,')
            line = line.replace(',Research,', ',Research Report,')
            line = line.replace(',Two Owner Search,', ',Two Owner Search Report,')
            line = line.replace(',Update Search,', ',Update Search Report,')            
            output.write(line)
        i += 1
    output.close()
