with open('RWClientFees.csv', 'r') as data:
    file = 1
    i = 1
    output = open('FixedRWClientFees1.csv', 'w')
    output.write('"PartnerType","Company","County","State","ProductType","Amount","FeeName","TransactionTypeID"')
    for line in data:
        if i >= 700000:
            output.close()
            file +=1
            i = 1
            output = open('FixedRWClientFees' + str(file) + '.csv', 'w')
            output.write('"PartnerType","Company","County","State","ProductType","Amount","FeeName","TransactionTypeID"')
        if 'Brg/Update Rec' in line or 'Doc Res/Rec' in line or 'Doc/Date Search' in line or 'Test' in line:
            pass
        elif 'Doc Ret' in line:
            new = line.replace('"Doc Ret",$', '"Document Retrieval",$').replace('"Doc Ret"', '"Document Retrieval",5')
            output.write(new)
            new = line.replace('"Doc Ret",$', '"Assignment Verification Report (AVR)",$').replace('"Doc Ret"', '"Assignment Verification Report (AVR)",5')
            output.write(new)
            if "CoreLogic, LLC (Advisory Solution)" in line:
                new = line.replace('"Doc Ret",$', '"SMR",$').replace('"Doc Ret"', '"SMR",6')
                output.write(new)
        elif "Mini Search" in line:
            new = line.replace('0,"Mini Search"', '0,"Mini Search",6')
            output.write(new)
            if "CoreLogic, LLC (Advisory Solution)" in line:
                new = line.replace('"Mini Search",$', '"MMR",$').replace('"Mini Search"', '"MMR",6')
                output.write(new)
        else:
            line = line.replace('"10 Yr Title Search",$', '"10 Year Search",$').replace('"10 Yr Title Search"', '"10 Year Search",6')
            line = line.replace('"20 Yr Title Search",$', '"20 Year Search",$').replace('"20 Yr Title Search"', '"20 Year Search",6')
            line = line.replace('"30 Yr Title Search",$', '"30 Year Search",$').replace('"30 Yr Title Search"', '"30 Year Search",6')
            line = line.replace('"C O Search",$', '"Current Owner Ownership & Encumbrance Report",$').replace('"C O Search"', '"Current Owner Ownership & Encumbrance Report",6')
            line = line.replace('"Certified Copy",$', '"Certified Document Retrieval",$').replace('"Certified Copy"', '"Certified Document Retrieval",5')
            line = line.replace('"Chain of Title Search",$', '"Chain of Title Report",$').replace('"Chain of Title Search"', '"Chain of Title Report",6')
            line = line.replace('"Civil Search - Case # Not Provided",$', '"Civil Copy Retrieval w/o Case #",$').replace('"Civil Search - Case # Not Provided"', '"Civil Copy Retrieval w/o Case #",5')
            line = line.replace('"Civil Search - Case # Provided",$', '"Civil Copy Retrieval w/case #",$').replace('"Civil Search - Case # Provided"', '"Civil Copy Retrieval w/case #", 5')
            line = line.replace('"Doc Rec",$', '"Walk-In Document Recording",$').replace('"Doc Rec"', '"Walk-In Document Recording",4')
            line = line.replace('"Full Title Search",$', '"Full Search",$').replace('"Full Title Search"', '"Full Search",6')
            line = line.replace('"HOA Identification",$', '"HOA Directory Search Report",$').replace('"HOA Identification"', '"HOA Directory Search Report",5')
            line = line.replace('"Judgment Search",$', '"Judgment & Lien Search Report",$').replace('"Judgment Search"', '"Judgment & Lien Search Report",6')
            line = line.replace('"Lien Release Report",$', '"Lien Release Search Report",$').replace('"Lien Release Report"', '"Lien Release Search Report",5')
            line = line.replace('"Mini Research",$', '"Mini Search No Copies",$').replace('"Mini Research"', '"Mini Search No Copies", 5')
            line = line.replace('"Research",$', '"Research Report",$').replace('"Research"', '"Research Report",5')
            line = line.replace('"Two Owner Search",$', '"Two Owner Search Report",$').replace('"Two Owner Search"', '"Two Owner Search Report",6')
            line = line.replace('"Upd Search",$', '"Update Search Report",$').replace('"Upd Search"', '"Update Search Report",6')
            output.write(line)
        i += 1
    output.close()
