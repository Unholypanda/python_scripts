import csv

with open('FixedRWVendorFees.csv', 'r') as infile, open('OutFile.csv', 'w', newline='') as outfile:
    reader = csv.DictReader(infile)
    writer = csv.writer(outfile, quoting=csv.QUOTE_ALL, lineterminator='\n')

    for row in reader:
        row['County'] = row['County'].split(',')[0]
        writer.writerow([row['Company'],row['County'],row['State'],row['Product'],row['Fee'],row['Grade'],row['Weight']])
