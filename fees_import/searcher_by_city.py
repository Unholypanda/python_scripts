import csv

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine("mssql+pyodbc://django:J@c0B1992@RWSQLServer/ResWare?driver=SQL+Server")
Base = declarative_base()
metadata = MetaData(engine)

class Partners(Base):
    __table__ = Table('PartnerCompany', metadata, autoload=True)


class Counties(Base):
    __table__ = Table('County', metadata, autoload=True)


class PartnerTypeRel(Base):
    __table__ = Table('PartnerCompanyPartnerTypeRel', metadata, autoload=True)


# ----------------------------------------------------------------------
def loadSession():
    """"""
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


if __name__ == "__main__":
    session = loadSession()
    partner_type_id = 10007
    with open('searcher_by_city_list.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            partner = session.query(Partners).filter_by(Name=row['company']).first()
            if not partner:
                print "No partner with name " + row['company']
            else:
                rel = PartnerTypeRel(PartnerCompanyID=partner.PartnerCompanyID, PartnerTypeID=partner_type_id)
                session.add(rel)
                session.commit()
                print str(rel.PartnerCompanyID) + " - " + str(rel.PartnerTypeID)