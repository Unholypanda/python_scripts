import csv

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker
 
class Partners(object):
    pass
	
class Counties(object):
    pass
 
#----------------------------------------------------------------------
def loadSession():
    """"""    
    engine = create_engine("mssql+pyodbc://django:J@c0B1992@RWSQLServer/ResWare?driver=SQL+Server")
 
    metadata = MetaData(engine)
    partner_company = Table('PartnerCompany', metadata, autoload=True)
    county = Table('County', metadata, autoload=True)
    mapper(Partners, partner_company)
    mapper(Counties, county)
	
    Session = sessionmaker(bind=engine)
    session = Session()
    return session
 
if __name__ == "__main__":
    session = loadSession()
    with open('county_errors.txt', 'w') as county_errorlog:
        with open('vendor_errors.txt', 'w') as vendor_errorlog:
            with open('vendor_fees.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    partner = session.query(Partners).filter_by(Name=row['company_name']).first()
                    if not partner:
                        print "Partner with name '" + row['company_name'] + "' not found."
                        vendor_errorlog.write("Partner with name '" + row['company_name'] + "' not found.\n")
                    else:
                        county = session.query(Counties).filter_by(State=row['state_code'], County=row['county_name']).first()
                        if not county:
                            print "County " + row['county_name'] + ", " + row['state_code'] + " not found"
                            county_errorlog.write("County " + row['county_name'] + ", " + row['state_code'] + " not found\n")
                        else:
                            print str(partner.PartnerCompanyID) + ', ' + str(county.CountyID) + ", " + str(row['product1']) + ", " + str(row['product2']) + ", " + str(row['fee1'])


# ledger = 7